//? Comparison Query Operators

// $gt/$gte
/* 
    - Allows us to find documents that have field number values greater than or equal to a specified field
    - Syntax:
    db.collectionName.find({field: {$gt: value}});
    db.collectionName.find({field: {$gte: value}});
*/

db.users.find({ age: { $gt: 50 } });
db.users.find({ age: { $gte: 21 } });

// $lt/$lte
/*
    - Allows us to find documents that have field number values less than or equal to a specified field
    - Syntax:
    db.collectionName.find({field: {$lt: value}});
    db.collectionName.find({field: {$lte: value}});
*/

db.users.find({ age: { $lt: 50 } });
db.users.find({ age: { $lte: 65 } });

// $ne
/*
    - Allows us to find documents that have field numbers not equal to a specified field
    - Syntax:
    db.collectionName.find({field: {$ne: value}});
*/

db.users.find({ age: { $ne: 82 } });

// $in
/* 
    - Allows us to find documents that have field values equal to any value in a specified array
    - Syntax:
    db.collectionName.find({field: {$in: [value1, value2, ...]}});
*/

db.users.find({ lastName: { $in: ["Hawking", "Doe"] } });
db.users.find({ courses: { $in: ["HTML", "React"] } });

// $nin
/*
    - Allows us to find documents that have field values not equal to any value in a specified array
    - Syntax:
    db.collectionName.find({field: {$nin: [value1, value2, ...]}});
*/

db.users.find({ courses: { $nin: ["HTML", "React"] } });

// Logical Query Operators

// $or
/*
    - Allows us to find documents that match a single criteria from multiple provided search criteria
    - Syntax:
    db.collectionName.find({$or: [{field1: value1}, {field2: value2}, ...]});
*/

db.users.find({ $or: [{ firstName: "Neil" }, { age: 21 }] });
db.users.find({ $or: [{ firstName: "Neil" }, { age: { $gt: 30 } }] });

// $and
/*
    - Allows us to find documents that match multiple criteria
    - Syntax:
    db.collectionName.find({$and: [{field1: value1}, {field2: value2}, ...]});
*/

db.users.find({ $and: [{ age: { $ne: 82 } }, { age: { $ne: 72 } }] });

// Field Projection
/* 
    - Retrieving documents are common operations that we do and by default, MongoDB returns the whole document
    - When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish
*/

// Inclusion
/*
    - Allows us to specify the fields that we want to included when we retrieve the document
    - Syntax:
    db.collectionName.find({}, {field1: 1, field2: 1, ...});
*/

db.users.find(
  {
    firstName: "Jane",
  },
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
  }
);

// Exclusion
/*
    - Allows us to specify the fields that we want to exclude when we retrieve the document
    - Syntax:
    db.collectionName.find({}, {field1: 0, field2: 0, ...});
*/

db.users.find(
  {
    firstName: "Jane",
  },
  {
    contact: 0,
    department: 0,
  }
);

db.users.insert({
  nameArr: [
    {
      nameA: "Juan",
    },
    {
      nameB: "Tamad",
    },
  ],
});

// The slice operator allows us to specify the number of elements that we want to return from an array
db.users.find(
  {
    nameArr: {
      nameA: "Juan",
    },
  },
  {
    nameArr: {
      $slice: 1,
    },
  }
);

// Evaluation Query Operators

// $regex
/*
    - Allows us to find documents that match a specified regular expression
    - Syntax:
    db.collectionName.find({field: {$regex: /pattern/}});
    db.collectionName.find({field: {$regex: 'pattern', $options: '<options>'}});
*/

// Case-sensitive query
db.users.find({ firstName: { $regex: "j"} });

// Case-insensitive query
db.users.find({ firstName: { $regex: "j", $options: "i" } });

// What does the $options do?
/*
    - Allows us to specify the options to apply to the regular expression
    - Options:
        - i: case-insensitive
        - m: multiline
        - x: can contain comments
        - s: dotall, matches all characters including newlines
        - u: unicode
        - g: global match
*/
